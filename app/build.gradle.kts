plugins {
    id("com.android.application")
    kotlin("android")
//    id("com.google.devtools.ksp")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("com.google.firebase.firebase-perf")
//    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdk = 35
    namespace = "org.kalabovi.pradelna"

    defaultConfig {
        applicationId = "org.kalabovi.pradelna"
        minSdk = 34
        targetSdk = 35
        versionCode = 2
        versionName = "0.1.1"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    buildFeatures {
        buildConfig = true
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
        freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
    }
    testOptions.unitTests.all { it.useJUnitPlatform() }
    packagingOptions.resources.excludes += "DebugProbesKt.bin"
}

dependencies {
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.1.5")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.10.1")

    implementation("io.coil-kt:coil-base:2.7.0")
    implementation("com.google.android.gms:play-services-mlkit-text-recognition:19.0.1")
    implementation("androidx.work:work-runtime-ktx:2.10.0")

    implementation("androidx.core:core-ktx:1.15.0")
    implementation("androidx.appcompat:appcompat:1.7.0")
//    implementation("androidx.fragment:fragment-ktx:1.6.0-alpha08")
    implementation("androidx.activity:activity-ktx:1.10.1")
//    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.1")
//    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.6.1")

    implementation("com.google.android.material:material:1.12.0")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.2.0-beta01")
//    implementation("androidx.constraintlayout:constraintlayout:2.2.0-alpha09")

//    implementation("com.google.android.play:app-update-ktx:2.0.1")

    implementation("com.jakewharton.timber:timber:5.0.1")

    val leakcanaryVersion = "2.14"
    debugImplementation("com.squareup.leakcanary:leakcanary-android:$leakcanaryVersion")
    implementation("com.squareup.leakcanary:plumber-android:$leakcanaryVersion")

    val koinVersion = "4.0.2"
    implementation("io.insert-koin:koin-android:$koinVersion")
    implementation("io.insert-koin:koin-androidx-workmanager:$koinVersion")

    implementation(platform("com.google.firebase:firebase-bom:33.10.0"))
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-perf-ktx")

//    val navVersion = "2.6.0-alpha08"
//    implementation("androidx.navigation:navigation-fragment-ktx:$navVersion")
//    implementation("androidx.navigation:navigation-ui-ktx:$navVersion")

    val kotestVersion = "6.0.0.M2"
    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
    val mockkVersion = "1.13.16"
    testImplementation("io.mockk:mockk-android:$mockkVersion")
    testImplementation("io.mockk:mockk-agent:$mockkVersion")
}
