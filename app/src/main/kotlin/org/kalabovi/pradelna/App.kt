package org.kalabovi.pradelna

import android.app.Application
import android.net.TrafficStats
import android.os.StrictMode
import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import coil.ImageLoader
import coil.request.CachePolicy
import coil.request.ImageRequest
import coil.util.DebugLogger
import com.google.android.material.color.DynamicColors
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.workmanager.dsl.workerOf
import org.koin.androidx.workmanager.koin.workManagerFactory
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import timber.log.Timber
import kotlin.time.Duration.Companion.minutes
import kotlin.time.toJavaDuration

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
        TrafficStats.setThreadStatsTag(SOCKET_TAG)
        Timber.plant(CrashlyticsTree())
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            FragmentManager.enableDebugLogging(true)
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@App)
            workManagerFactory()
            if (BuildConfig.DEBUG) {
                androidLogger(Level.DEBUG)
            }
            modules(
                module {
                    single { TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS) }
                    single {
                        ImageLoader.Builder(androidContext())
                            .memoryCachePolicy(CachePolicy.DISABLED)
                            .apply {
                                if (BuildConfig.DEBUG) {
                                    logger(DebugLogger(Log.VERBOSE))
                                }
                            }
                            .build()
                    }
                    single {
                        ImageRequest.Builder(androidContext())
                            .data("http://pradelna.local")
                            .build()
                    }
                    workerOf(::Work)
                }
            )
        }

        WorkManager.getInstance(this).enqueueUniquePeriodicWork(
            "Photo",
            ExistingPeriodicWorkPolicy.CANCEL_AND_REENQUEUE,
            PeriodicWorkRequestBuilder<Work>(5.minutes.toJavaDuration())
                .setConstraints(
                    Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build()
                )
                .build()
        )
    }

    private companion object {
        private const val SOCKET_TAG = 42
    }
}
