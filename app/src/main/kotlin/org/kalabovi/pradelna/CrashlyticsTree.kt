package org.kalabovi.pradelna

import android.util.Log
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import timber.log.Timber

class CrashlyticsTree : Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        Firebase.crashlytics.log(
            buildString {
                append(priority.priorityChar)
                append('/')
                append(tag)
                append(": ")
                append(message)
            }
        )
        if (t != null) {
            Firebase.crashlytics.recordException(t)
        }
    }

    private val Int.priorityChar: Char
        get() = when (this) {
            Log.VERBOSE -> 'V'
            Log.DEBUG -> 'D'
            Log.INFO -> 'I'
            Log.WARN -> 'W'
            Log.ERROR -> 'E'
            Log.ASSERT -> 'A'
            else -> '?'
        }
}
