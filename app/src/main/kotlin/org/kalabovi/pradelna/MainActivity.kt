package org.kalabovi.pradelna

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.ImageLoader
import coil.request.ImageRequest
import com.google.firebase.perf.trace
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognizer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.ScopeActivity
import timber.log.Timber
import kotlin.time.Duration.Companion.minutes

class MainActivity : ScopeActivity(R.layout.main) {

    private val image: ImageView by lazy { findViewById(R.id.image) }
    private val text: TextView by lazy { findViewById(R.id.text) }
    private val swipeRefresh: SwipeRefreshLayout by lazy { findViewById(R.id.swiperefresh) }
    private val recognizer: TextRecognizer by inject()
    private val request: ImageRequest by inject()
    private val imageLoader: ImageLoader by inject()

    private val requestPermissionLauncher by lazy {
        registerForActivityResult(RequestPermission()) {
            Timber.d("Granted: $it")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById<FrameLayout>(R.id.frame)) { v, wi ->
            val i = wi.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(i.left, i.top, i.right, i.bottom)
            wi
        }
        when (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS)) {
            PackageManager.PERMISSION_GRANTED -> Timber.d("Permission granted")
            else -> requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
        swipeRefresh.setOnRefreshListener(::refresh)
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                while (isActive) {
                    refresh()
                    delay(1.minutes)
                }
            }
        }
    }

    private fun refresh() {
        imageLoader.enqueue(
            ImageRequest.Builder(request)
                .target(image)
                .listener(
                    onStart = { swipeRefresh.isRefreshing = true },
                    onCancel = { swipeRefresh.isRefreshing = false },
                    onError = { _, result ->
                        Timber.w(result.throwable)
                        text.text = result.throwable.message
                        swipeRefresh.isRefreshing = false
                    },
                    onSuccess = { _, result ->
                        lifecycleScope.launch {
                            text.text = withContext(Dispatchers.Default) {
                                trace("recognizer") {
                                    recognizer.process(
                                        InputImage.fromBitmap(result.drawable.toBitmap(), 0)
                                    )
                                }.await().text
                            }
                            swipeRefresh.isRefreshing = false
                        }
                    }
                )
                .build()
        )
    }
}
