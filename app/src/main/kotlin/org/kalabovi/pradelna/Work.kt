package org.kalabovi.pradelna

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.graphics.drawable.toBitmap
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import coil.ImageLoader
import coil.request.ErrorResult
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.google.firebase.perf.trace
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognizer
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import java.net.UnknownHostException

class Work(
    appContext: Context,
    params: WorkerParameters,
    private val recognizer: TextRecognizer,
    private val request: ImageRequest,
    private val imageLoader: ImageLoader
) : CoroutineWorker(appContext, params) {
    private val notificationManager = NotificationManagerCompat.from(appContext)

    init {
        try {
            notificationManager.createNotificationChannel(
                NotificationChannelCompat.Builder(
                    NOTIFICATION_CHANNEL_ID,
                    NotificationManagerCompat.IMPORTANCE_LOW
                ).setName("Running").build()
            )
        } catch (ce: CancellationException) {
            throw ce
        } catch (e: Exception) {
            Timber.w(e)
        }
    }

    override suspend fun doWork() = when (val result = imageLoader.execute(request)) {
        is SuccessResult -> {
            try {
                val bmp = result.drawable.toBitmap()
                val text = trace("recognizer") {
                    recognizer.process(InputImage.fromBitmap(bmp, 0))
                }.await()
                Timber.d(text.text)
                val notification =
                    NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.drawable.laundry)
                        .setContentText(text.text)
                        .setLargeIcon(bmp)
                        .setOngoing(true)
                        .setStyle(
                            NotificationCompat.BigPictureStyle()
                                .bigPicture(bmp)
                                .bigLargeIcon(null as Bitmap?)
                        )
                        .setContentIntent(
                            TaskStackBuilder.create(applicationContext)
                                .addNextIntentWithParentStack(
                                    Intent(applicationContext, MainActivity::class.java)
                                ).getPendingIntent(
                                    0,
                                    PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                                )
                        )
                        .build()
                notificationManager.notify(NOTIFICATION_ID, notification)
            } catch (ce: CancellationException) {
                throw ce
            } catch (se: SecurityException) {
                Timber.w(se)
            } catch (e: Exception) {
                Timber.w(e)
            }
            Result.success()
        }

        is ErrorResult -> {
            Timber.w(result.throwable)
            if (result.throwable is UnknownHostException) {
                notificationManager.cancel(NOTIFICATION_ID)
            }
            Result.retry()
        }
    }

    private companion object {
        private const val NOTIFICATION_CHANNEL_ID = "RUNNING"
        private const val NOTIFICATION_ID = 42
    }
}
