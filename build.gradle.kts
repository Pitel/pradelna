// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    val kotlin = "2.1.10"
    id("com.android.application") version "8.8.2" apply false
    id("com.android.library") version "8.8.2" apply false
    id("org.jetbrains.kotlin.android") version kotlin apply false
    id("com.google.devtools.ksp") version "$kotlin-1.0.31" apply false
    id("com.google.gms.google-services") version "4.4.2" apply false
    id("com.google.firebase.crashlytics") version "3.0.3" apply false
    id("com.google.firebase.firebase-perf") version "1.4.2" apply false
    id("androidx.navigation.safeargs.kotlin") version "2.8.7" apply false
}
